- name: Login to GitLab repository locally
  local_action:
    module: docker_login
    registry: "{{ gitlab_registry_url }}"
    username: "{{ gitlab_deploy_user }}"
    password: "{{ gitlab_deploy_token }}"

- name: Build database Docker image. Uses random numbers to force Docker to build certain parts of image every time
  local_action:
    module: docker_image
    source: build
    build:
      args:
        CACHEBUST: "{{ 9999999 |random }}"
      path: "{{ role_path }}/files/database_docker"
    name: "allu_database:{{ branch }}"
    force_source: true

- name: Push built allu_database Docker image to GitLab repository
  local_action:
    module: docker_image
    push: true
    source: local
    force_tag: true  # As branch may already be present, we need to enable overwriting of existing tags
    name: "allu_database:{{ branch }}"
    repository: "{{ gitlab_registry_url }}/vincit/allu/allu/allu_database:{{ branch }}"

- name: Start Docker services on remote host
  service: name=docker state=started
  become: yes

- name: Stop and remove existing allu_database container from Docker
  docker_container:
    name: "{{ database_container_name }}"
    state: absent
  become: yes

- name: Remove existing allu_database image from Docker
  docker_image:
    name: "{{ gitlab_registry_url }}/vincit/allu/allu/allu_database:{{ branch }}"
    state: absent
    force_absent: true
  become: yes
  when: "'test' in group_names or 'staging' in group_names"

- name: Login to GitLab repository at the remote
  docker_login:
    registry: "{{ gitlab_registry_url }}/vincit/allu/allu"
    username: "{{ gitlab_deploy_user }}"
    password: "{{ gitlab_deploy_token }}"
  become: yes

- name: Pull allu_database Docker image from GitLab repository
  docker_image:
    source: pull
    name: "{{ gitlab_registry_url }}/vincit/allu/allu/allu_database:{{ branch }}"
  become: yes

- name: Start allu_database container
  docker_container:
    name: "{{ database_container_name }}"
    image: "{{ gitlab_registry_url }}/vincit/allu/allu/allu_database:{{ branch }}"
    shm_size: "{{ container_shm_size }}"
    restart_policy: always
    env:
      POSTGRES_PASSWORD: "{{ database_password_postgres }}"
    volumes:
      - /srv/data/allu/database/data:/var/lib/postgresql/data:rw
    published_ports:
      - "5432:5432"
    networks:
      - name: "{{ network_name }}"
        aliases:
          - databasecontainer
  become: yes

- name: wait for Postgresql to initialize
  postgresql_ping:
    login_host: localhost
    port: 5432
    login_user: postgres
    login_password: "{{ database_password_postgres }}"
  register: result
  until: result.is_available
  retries: 30
  delay: 4
