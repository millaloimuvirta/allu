Hei
Viestin liitteenä on aluevuokraukseen liittyvä päätös ${attachmentName} liitteineen.
Tutustukaa päätöksen sisältöön huolellisesti ja ottakaa yhteyttä, mikäli teillä on kysyttävää tai huomautettavaa siihen liittyen.

Miten onnistuimme? Kerro mielipiteesi täällä: https://response.questback.com/helsinginkaupunki/mkqgmxkxxv

Terveisin

<#if handlerName??>${handlerName}</#if>

Helsingin kaupunki
Kaupunkiympäristön asukas- ja yrityspalvelut
Alueidenkäyttö
Puh. 09 310 22111
luvat@hel.fi
www.hel.fi/luvat/yleisetalueet
